import React from 'react'
import { useSelector } from 'react-redux'
import { Task } from '../../../types/Task'
import { TodoState } from '../reducers'
interface TodoViewProps{
  todos:Task[],
  // todos2:Task[],
  DeleteItem:(id:number)=>void
}

const TodoView:React.FC<TodoViewProps> = ({DeleteItem,todos}) => {
  return (
    <div className='container-ListItems'>
        <h1>Todo List</h1>
        <div className='ListItems'>
        <div >
          <span className="task title">Task</span>
          <span className="description title">Description</span>
          <span className="createdAt title">At</span>
          <span className="action title">Action</span>
      </div>
        {todos?.map((ele,index)=>(
           <div key={index}>
           {/* <span>{item.id}</span> */}
           <span className="task">{ele.task} </span>
           <span className="description">{ele.description}</span>
           <span className="created-at">{ele.createdAt? ele.createdAt.slice(0, 10) + ' ' + ele.createdAt.slice(11, 16) : ''}</span>
           <span className="action icon" onClick={()=>DeleteItem(index)}>&times;</span>
           </div>
        ))}
           </div>

    </div>
  )
}

export default TodoView