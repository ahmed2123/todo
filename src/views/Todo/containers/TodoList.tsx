import React, { useEffect, useState } from 'react';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import AddTodo from '../components/AddTodo';
import TodoView from './TodoView';
import { useDispatch, useSelector } from 'react-redux';
import {deleteItemAction} from "../actions"
import { TodoState } from '../reducers';
import TaskService from '../../../services/TasksService';

const useStyles = makeStyles(theme =>
  createStyles({
    root: {},
  })
);
const service = new TaskService()
const TodoList = () => {
  const classes = useStyles();
  
  const todos = useSelector((state:TodoState)=>state.todos)

  const dispatch =useDispatch();
  
  const DeleteItem=(id:number)=>{
      dispatch(deleteItemAction(id))
  }
  return (
    <div className='container'>
      <AddTodo />

    { todos.length>0 && <TodoView todos={todos}  DeleteItem={DeleteItem}/>}
    </div>
  );
};

export default TodoList;
