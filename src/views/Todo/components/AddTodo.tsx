import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { Button, TextField } from '@material-ui/core';
import { useDispatch } from 'react-redux';
import { addTask } from '../actions';
import TaskService from '../../../services/TasksService';
import {Task} from "../../../types/Task"

const useStyles = makeStyles(() =>
  createStyles({
    root: {},
  })
);

const AddTodo: React.FC = () => {
  const classes = useStyles();
  const service = new TaskService();
  const dispatch = useDispatch();
  const moment = require('moment');

  // Define validation schema
  const validationSchema = Yup.object().shape({
    task: Yup.string().required('Required').matches(/^[a-zA-Z]+$/, "Task can only contain letters"),
    description: Yup.string().required('Description is required').min(20,"at least twenty characters"),
  });

  // Initialize Formik
  const formik = useFormik({
    initialValues: {
      id:'',
      task: '',
      description: '',
    },
    validationSchema,
    onSubmit: ({id,task,description}:Task) => {
      const timestamp = moment().format("YYYY-MM-DD HH:mm:ss");
      const taskWithTimestamp = {...{ id, task, description }, createdAt: timestamp };

      dispatch(addTask(taskWithTimestamp));

      service.addTask(taskWithTimestamp);
      formik.resetForm(); // Reset form after submission
    },
  });

  return (
    <div className="AddTodo">
      <form onSubmit={formik.handleSubmit}>
        <TextField
        className='field'
          fullWidth
          margin="normal"
          id="addTaskInput"
          label="Title"
          variant="outlined"
          autoFocus
          {...formik.getFieldProps('task')}
          name="task"
        />
        {formik.touched.task && formik.errors.task? (
          <div style={{ color: 'red'  ,width:'100%'}}>{formik.errors.task}</div>
        ) : null}

        <TextField
          fullWidth
          margin="normal"
          id="addTaskDesc"
          label="Description"
          variant="outlined"
          autoFocus
          {...formik.getFieldProps('description')}
          name="description"
        />
        {formik.touched.description && formik.errors.description? (
          <div style={{ color: 'red' }}>{formik.errors.description}</div>
        ) : null}

        <Button type="submit" variant="contained">
          Add Task
        </Button>
      </form>
    </div>
  );
};

export default AddTodo;
