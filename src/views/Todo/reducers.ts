import { TodoActionTypes } from './actions';
import update from 'immutability-helper';
import { combineReducers } from 'redux';
import { Task } from '../../types/Task';
import TaskService from '../../services/TasksService';

export interface TodoState{
  todos:Task[]
} 
  const initialState ={
    todos:[]
  }
  const service =new TaskService();
export const todoReducer = (state = initialState, action: any) => {
  switch (action.type) {
    case "ADD_TASK":
      let newState = {...state, todos: [...state.todos, action.payload]};
      return newState

      case "DELETE_TASK":
      let newState2 = {...state,todos:[...state.todos.filter((item,index)=>index!=action.payload)]}
      let val=action.payload+1
      service.removeTask(val)
      return newState2
    default:
      return state;
  }
};

const reducer = combineReducers({
  current: todoReducer,
});

export default todoReducer;
