import { Dispatch } from 'redux';
import { Task } from '../../types/Task';
export type Action = { 
  type : string , payload:object |number
}

export const TodoActionTypes = {
  addTodo: 'TODO/ADD',
  deleteTodo: 'TODO/REMOVE',
};
export const addTask=(obj:Task):Action=>({
  type:"ADD_TASK",
  payload:obj
})
export const deleteItemAction=(id:number):Action=>({
  type:"DELETE_TASK",
  payload:id
})
