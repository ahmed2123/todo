Firstly, I would like to express my gratitude to your company for providing me with this opportunity. Secondly, the project version I received was outdated, which led to numerous issues during the installation process. Despite having worked on two to three projects in typescript as you may know, the project proved to be somewhat complex due to many reducer files resulting from overlapping elements. Therefore, I established a store specifically for the Todo project, and I will share the steps I took : 

1- create TodoView Component to show the tasks and delete it

2- i edit some changes on actions.tsx file so i make it without class just functions

3- in AddTodo Component i store each element i create in localStorage , i used formik and yup libraries to make some restrictions on the fields also , i used memo library to send what time i create this task and show it in TodoView 

4- in  TodoView Component i show the task , description , date and delete option , and each deleted element it will also delete from localStorage so i used TaskService file

5-  i made some edits on reducers.ts file  
   

Note : Despite being in the Eid period and having limited time and peace of mind, I managed to complete this project with pure css and with not using useCustom to makeStyle . I hope it meets your approval , and thank you once again for your generous support .
